#!/usr/bin/python3

import shutil,csv
import sys, subprocess
import cgi, cgitb 
cgitb.enable()

# Create instance of FieldStorage 
form = cgi.FieldStorage() 

# Get data from fields
par1 = form.getvalue('param1')
par2 = form.getvalue('param2')
par3 = form.getvalue('param3')

print("Content-Type: text/html")    # HTML is following
print()                             # blank line, end of headers

print("<TITLE>CGI script output</TITLE>")
print("<H1>This is my first CGI script</H1>")
print("Hello, world!")
print("<p>param1:", form["param1"].value)
print("<p>param2:", form["param2"].value)
print("<p>param3:", form["param3"].value)

shifter_list="/afs/cern.ch/user/s/ssottoco/www/otp_checks/data/persons_RC.csv"
#Make first a copy of the file
copy = open(shifter_list+"~", "w")
dest = open(shifter_list, "r")
for line in dest:
  copy.write(line)
copy.close()
dest.close()

#Proceed replacing the changed row[] 
source= open( shifter_list+"~", "r" )
destination= open( shifter_list, "w" )

# for row in source:
  # print(row)
  # destination.write(row)

csv_reader = csv.reader(source, delimiter=',')
for row in csv_reader:
  string=""
  print("<p>processing: -", row[0], "- param: -", par1, "-")
  if row[0] == par1[1:-1]:
    row[int(par3)-1]= par2
    print("<p>Found it! new values for row[", par3, "] is ", par2)
  for i in row:
    string = string + i + "," 
  string= string + "\n"
  destination.write(string)
source.close()
destination.close()

#subprocess.call("/afs/cern.ch/user/s/ssottoco/www/otp_checks/generatehtml.sh")
