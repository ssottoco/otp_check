#!/usr/bin/env tdaq_python

from __future__ import division
from __future__ import print_function
from past.utils import old_div
import sys

from otptask import OTPTask
from datetime import datetime, date
from collections import defaultdict

import cgitb
cgitb.enable()


def header():

    # print "Content-type:text/html\r\n\r\n"
    pass


def weindex(total, we):

    try:
        return '%.2f' % (old_div((old_div(1.*we,total)),(2./7)))
    except ZeroDivisionError as e:
        return '--' if we == 0 else 'inf'


def format_shifters(persons):

    print('<br>')
    print('<br>')
    print('<table border="1" style="width:50%">')

    print('<tr>')
    print('<td>', 'Name', '</td>')
    print('<td>', 'Total number of shifts (days)', '</td>')
    print('<td>', 'Weekend/Holidays (days)', '</td>')
    print('<td>', 'Weekend index', '</td>')
    print('</tr>')

    for p in sorted(persons.keys()):
        print('<tr>')
        print('<td>', p, '</td>')
        print('<td>', persons[p][0], '</td>')
        print('<td>', persons[p][1], '</td>')
        print('<td>', weindex(*persons[p]), '</td>')
        print('</tr>')

    print('</table>')


def format_institutes(institutes):

    print('<br>')
    print('<br>')
    print('<table border="1" style="width:50%">')

    print('<tr>')
    print('<td>', 'Institute', '</td>')
    print('<td>', 'Total number of shifts (days)', '</td>')
    print('<td>', 'Weekend/Holidays (days)', '</td>')
    print('<td>', 'Share', '</td>')
    print('</tr>')

    total = sum(v[0] for v in list(institutes.values()))

    for p in sorted(institutes.keys()):
        inst = p if p != "London RHBNC" else "RHUL"
        print('<tr>')
        print('<td>', inst, '</td>')
        print('<td>', institutes[p][0], '</td>')
        print('<td>', institutes[p][1], '</td>')
        print('<td>', '%.2f' % (old_div(1.*institutes[p][0],total)), '</td>')
        print('</tr>')

    print('</table>')


def format_gaps(gaps):

    print('<br>')
    print('Incoming gaps in booking:', '<br>')

    for g in gaps:
        print(', '.join(e.strftime("%d-%m-%Y") for e in g))
        print('<br>')

    print('<br>')


def start_html(start, end, task):
    print('<html>')
    print('<head>')
    print('<title>', task, '</title>')
    print('</head>')
    print('<body>')
    print('<br>')
    print('Period', start.strftime("%d-%m-%Y"), '-->', end.strftime("%d-%m-%Y"))
    print('<br>')


def end_html():
    print('<br>')
    print('Generated', datetime.now())
    print('</body>')
    print('</html>')


def isweekend(d):

    weekend = False

    weekend |= d.weekday() > 4

    # Friday, 14th April                      (Good Friday)
    # Monday, 17th April                   (Easter Monday)
    # Monday, 1st May                        (1st May)
    # Thursday, 25th May                  (Ascension day)
    # Monday, 5th June                      (Whit Monday)
    # Thursday, 7th September         ("Jeune genevois")

    cernholidays = (date(2021, 4, 14), date(2021, 4, 17), date(2021, 5, 1),
                    date(2021, 5, 25), date(2021, 6, 5), date(2021, 9, 7))

    weekend |= d.date() in cernholidays

    # The Laboratory will be closed from Saturday, 23rd December 2017
    # to Sunday 7th January 2018 inclusive (without deduction of annual leave).
    # The first working day in the New Year will be Monday, 8th January 2018.

    weekend |= d.date() >= date(2021, 12, 23)

    return weekend


if __name__ == '__main__':

    start = datetime(2021, 1, 1)
    end = datetime(2021, 12, 31)

    task = sys.argv[1]
    requirement = '' if len(sys.argv) < 3 else sys.argv[2]

    oncall = OTPTask(task, requirement)

    shifters = oncall.shifters()

    institute = defaultdict(lambda: [0, 0])
    persons = defaultdict(lambda: [0, 0])
    for shifter in shifters:
        shifts = oncall.shifts(shifter['ID'], start, end)
        sname = shifter['LAST_NAME']+','+shifter['FIRST_NAME']
        persons[sname][0] = len(shifts)

        for s in shifts:
            institute[s['ONAME']][0] += 1
            if isweekend(s['DT']):
                institute[s['ONAME']][1] += 1
                persons[sname][1] += 1

    gaps = oncall.gaps(end)

    header()

    start_html(start, end, task)

    format_institutes(institute)
    format_shifters(persons)
    format_gaps(gaps)

    end_html()
