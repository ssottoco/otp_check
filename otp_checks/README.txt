# deployment on atlasdaq

sudo crontab -l
* * * * *  /var/www/html/otp_checks/checkfile.sh
00 5 * * * /var/www/html/otp_checks/generatehtml.sh

checkfile.sh - check if file /var/www/html/otp_checks/data/persons.csv has been modified. If yes, it is running the /var/www/html/otp_checks/generatehtml.sh
generatehtml.sh - generate static html pages for RC shifters, TRG shifters and the former DAQ on-call page doen by Wainer.
data/persons.csv - contains list of shifters and their qualification criterias as following:

## NAME, EXPERT, SIR RC, SIR TRG, SIR RC shadow, SIR TRG shadow
 RAMIREZ MORALES ANDRES ,No,Yes,Yes,Yes,,
 TRESOLDI FABIO ,Yes,Yes,Yes,,,
File has to have certain privileges, otherwise it will not be updated by the cgi-bin script
-rw-r--r--. 1 apache apache 1507 Jun  5 12:58 persons.csv

/cgi-bin/UpdateTraining.py - update via webpage of the records file /var/www/html/otp_checks/data/persons.csv. Ajax functionality is used to call the cgi script
ile has to have certain privileges as apache user, otherwise will not run.
-rwxrwxr-x. 1 root apache 1949 May  3 17:51 UpdateTraining.py
 

See here for the underlying OTP public API:

https://twiki.cern.ch/twiki/bin/viewauth/Atlas/OtpPublicApi


