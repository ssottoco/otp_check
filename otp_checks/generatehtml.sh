#!/bin/sh

source /afs/cern.ch/user/s/ssottoco/_l/tdaq_setup.sh > /dev/null 2>&1 

d=$(dirname $0)

outdir=/afs/cern.ch/user/s/ssottoco/www/otp_checks/
#outdir=/var/www/html/otp_checks

tdaq_python $d/webreport.py "DAQ/HLT on-call" "DAQ/HLT on-call"> $outdir/daqhltoncall.html
tdaq_python $d/webreport.py "ATLAS Run Control Shifter (ACR)" "ATLAS Run Control Shifter (ACR)"> $outdir/rc.html

#tdaq_python $d/shiftreport.py "ATLAS Run Control Shifter (ACR)" "ATLAS Trigger Shifter (ACR)" "Shadow Run Control Shifter (ACR)" "Shadow Trigger Shifter in ACR" > $outdir/RCshifters.html
#tdaq_python $d/shiftreport-display.py "ATLAS Run Control Shifter (ACR)" "ATLAS Trigger Shifter (ACR)" "Shadow Run Control Shifter (ACR)" "Shadow Trigger Shifter in ACR" > $outdir/DISPLAY-RCshifters.html

#tdaq_python $d/shiftreport-tmp.py "ATLAS Run Control Shifter (ACR)" "ATLAS Trigger Shifter (ACR)" "Shadow Run Control Shifter (ACR)" "Shadow Trigger Shifter in ACR" "2017" "240"> $outdir/RC-2017.html
#tdaq_python $d/shiftreport-tmp.py "ATLAS Run Control Shifter (ACR)" "ATLAS Trigger Shifter (ACR)" "Shadow Run Control Shifter (ACR)" "Shadow Trigger Shifter in ACR" "2018" "180"> $outdir/RC-2018.html
tdaq_python $d/shiftreport-tmp.py "ATLAS Run Control Shifter (ACR)" "ATLAS Trigger Shifter (ACR)" "Shadow Run Control Shifter (ACR)" "Shadow Trigger Shifter in ACR" "2021" "180"> $outdir/RC-2021.html

#tdaq_python $d/shiftreport-tmp.py "ATLAS Trigger Shifter (ACR)" "ATLAS Run Control Shifter (ACR)" "Shadow Trigger Shifter in ACR" "Shadow Run Control Shifter (ACR)" "2017" "240"> $outdir/TRG-2017.html
#tdaq_python $d/shiftreport-tmp.py "ATLAS Trigger Shifter (ACR)" "ATLAS Run Control Shifter (ACR)" "Shadow Trigger Shifter in ACR" "Shadow Run Control Shifter (ACR)" "2018" "180"> $outdir/TRG-2018.html

#tdaq_python $d/shiftreport.py "ATLAS Trigger Shifter (ACR)" "ATLAS Run Control Shifter (ACR)" "Shadow Trigger Shifter in ACR" "Shadow Run Control Shifter (ACR)" > $outdir/TRGshifters.html
#tdaq_python $d/shiftreport-display.py "ATLAS Trigger Shifter (ACR)" "ATLAS Run Control Shifter (ACR)" "Shadow Trigger Shifter in ACR" "Shadow Run Control Shifter (ACR)" > $outdir/DISPLAY-TRGshifters.html

