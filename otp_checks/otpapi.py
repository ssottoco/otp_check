#!/usr/bin/env tdaq_python

from builtins import zip
from builtins import object
import cx_Oracle

from operator import itemgetter

defaults = {
    "dsn": "CERNDB1",
    "user": "ppt_mao_pub",
    "password": "N4pn9n5545k3",
    }


def _get_headers(cursor):
    return [i[0] for i in cursor.description]


def _get_entries(cursor):
    """Transform the DB query result into a list of dictionaries.
    Keys are the column names.

    """

    headers = _get_headers(cursor)
    entries = cursor.fetchall()
    return [dict(zip(headers, e)) for e in entries]


class OTPAPI(object):

    def __init__(self, user=None, password=None, dsn=None):
        self.connection = None
        self.cursor = None
        self._connect(user, password, dsn)

    def _connect(self, user, password, dsn):
        connection_params = dict(defaults)
        # TO DO: udpate connection parameters with user provided
        self.connection = cx_Oracle.connect(**connection_params)
        self.cursor = self.connection.cursor()

    def get_shifters(self, task_name, req_name=None):
        """Find the people who can take a given type of shift.
        Returns a list of dicts:

        [{'FIRST_NAME': 'Name', 'LAST_NAME': 'Surname', 'ID': 9999, 'EMAIL': 'Email'}, ... ]
        """

        cmd = "select PUB_PERSON.FIRST_NAME,"\
            " PUB_PERSON.LAST_NAME, PUB_PERSON.ID, PUB_PERSON.EMAIL"\
            " from PUB_TASK, PUB_PERSONNEL, PUB_PERSON " +\
            (", PUB_RES_REQUIREMENT" if req_name else '') +\
            " where PUB_TASK.SHORT_TITLE = :title"\
            " and PUB_PERSONNEL.TASK_ID = PUB_TASK.ID "\
            " and PUB_PERSON.ID = PUB_PERSONNEL.PERSON_ID"
        keys = {'title': task_name}

        if req_name:
            cmd = cmd +\
                " and"\
                " PUB_PERSONNEL.RES_REQUIREMENT_ID = PUB_RES_REQUIREMENT.ID"\
                " and PUB_RES_REQUIREMENT.TITLE = :req"
            keys['req'] = req_name

        self.cursor.execute(cmd, keys)
        return _get_entries(self.cursor)

    def get_shifts(self,
                   shifter, task_name, start_date, end_date, req_name=None):
        """Find a person's shifts.
        Returns a list of dicts:

        [{'ALLOCATED_PERSON_ID': 9999, 'DT': datetime.datetime(2015, 2, 16, 0, 0), 'HOUR_FROM': 15, 'HOUR_TO': 23, 'ONAME': 'CERN'}, ...]
        """

        if req_name:
            cmd = "select PUB_RES_REQUIREMENT.ID"\
                " from PUB_TASK,PUB_RES_REQUIREMENT"\
                " where PUB_RES_REQUIREMENT.TITLE = :req"\
                " and PUB_TASK.SHORT_TITLE = :title"\
                " and PUB_TASK.ID = PUB_RES_REQUIREMENT.TASk_ID"
            keys = {'title': task_name, 'req': req_name}
            self.cursor.execute(cmd, keys)
            req_id = self.cursor.fetchone()[0]

        cmd = "select PUB_SUM_ALLOCATION_V.DT,"\
            " PUB_SUM_ALLOCATION_V.HOUR_FROM,"\
            " PUB_SUM_ALLOCATION_V.HOUR_TO,"\
            " PUB_SUM_ALLOCATION_V.ALLOCATED_PERSON_ID,"\
            " PUB_INSTITUTE.ONAME"\
            " from PUB_TASK, PUB_SUM_ALLOCATION_V, PUB_INSTITUTE"\
            " where PUB_TASK.ID = PUB_SUM_ALLOCATION_V.TASK_ID"\
            " and PUB_SUM_ALLOCATION_V.ALLOCATED_PERSON_ID = :shifter"\
            " and PUB_INSTITUTE.ID ="\
            " PUB_SUM_ALLOCATION_V.ALLOCATED_PERSON_INSTITUTE_ID"\
            " and PUB_TASK.SHORT_TITLE = :title"\
            " and TRUNC(DT) between :tstart "\
            " and :tend "
        keys = {'title': task_name, 'shifter': shifter,
                'tstart': start_date, 'tend': end_date}

        if req_name:
            cmd = cmd + " and PUB_SUM_ALLOCATION_V.RES_REQUIREMENT_ID = :req"
            keys['req'] = req_id

        self.cursor.execute(cmd, keys)
        return _get_entries(self.cursor)

    def get_coverage(self, task, start_date=None, end_date=None):
        """Query the database for scheduled shifts
        Returns a list of dicts:

        [{'FIRST_NAME': 'Name', 'LAST_NAME': 'Surname', 'SHORT_TITLE': 'Task name', 'ID': 9999, 'HOUR_FROM': 15, 'HOUR_TO': 23, 'DT': datetime.datetime(2015, 8, 4, 0, 0), 'EMAIL': 'name.surname@cern.ch'}, ...]
        """

        output = []
        cmd = "select PUB_TASK.SHORT_TITLE, PUB_SUM_ALLOCATION_V.DT,"\
            " PUB_SUM_ALLOCATION_V.HOUR_FROM,"\
            " PUB_SUM_ALLOCATION_V.HOUR_TO,"\
            " PUB_PERSON.FIRST_NAME, PUB_PERSON.LAST_NAME,"\
            " PUB_PERSON.EMAIL, PUB_PERSON.ID"\
            " from PUB_TASK, PUB_SUM_ALLOCATION_V, PUB_PERSON"\
            " where PUB_TASK.ID = PUB_SUM_ALLOCATION_V.TASK_ID"\
            " and PUB_SUM_ALLOCATION_V.ALLOCATED_PERSON_ID = PUB_PERSON.ID"\
            " and PUB_TASK.SHORT_TITLE = :title"\
            " and TRUNC(DT) between :tstart "\
            " and :tend"
        keys = {'title': task, 'tstart': start_date, 'tend': end_date}

        self.cursor.execute(cmd, keys)
        return _get_entries(self.cursor)

if __name__ == "__main__":

    otpapi = OTPAPI()
