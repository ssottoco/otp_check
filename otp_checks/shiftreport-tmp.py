#!/usr/bin/env tdaq_python

from __future__ import print_function
from builtins import str
import sys

from otptask import OTPTask
from datetime import datetime, date, timedelta
from collections import defaultdict

import csv
import cgitb
cgitb.enable()


def header():

    # print "Content-type:text/html\r\n\r\n"

    pass

def format_shifters(persons):

    print('<br>')
    print('<table id="report" border="1" cellspacing="0" cellpadding="3" style="font-size:12 ; width:100%">')
    print('<tr>')
    print('<th onclick="sortTable(0)" style="cursor:pointer"><b>Name</b></th>')
    print('<th><b>Email</b></th>')
    print('<th><b>2021 shifts</b></th>')
    print('<th><b>Trained</b></th>')
    print('<th><b>SIR RC</b></th>')
    print('<th><b>SIR TRG</b></th>')
    #print '<th><b>SIR shadow RC</b></th>'
    #print '<th><b>SIR shadow TRG</b></th>'
    print('<th><b>Last RC shadow shift</b></th>')
    print('<th><b>Last TRG shadow shift</b></th>')
    print('<th><b>First shift</b></th>')
    print('<th><b>Last shift</b></th>')
    print('<th><b>Done</b></th>')
    print('<th><b>Total</b></th>')
    print('<th><b>Qualification (RC+TRG)</b></th>')
    print('</tr>')
    for p in sorted(persons.keys()):
        print('<tr bgcolor="#f6f6f6">')
        print('<td nowrap>', p, '</td>') 
        print('<td nowrap>', persons[p][0], '</td>')
        print('<td onclick="updateTraining()" style="cursor:pointer">',persons[p][13],'</td>')
        print('<td onclick="updateTraining()" style="cursor:pointer">',persons[p][8],'</td>')
        print('<td onclick="updateTraining()" style="cursor:pointer">',persons[p][9],'</td>')
        print('<td onclick="updateTraining()" style="cursor:pointer">',persons[p][10],'</td>')
        #print '<td onclick="updateTraining()" style="cursor:pointer">',persons[p][11],'</td>'
        #print '<td onclick="updateTraining()" style="cursor:pointer">',persons[p][12],'</td>'
        if persons[p][3] == 0:
            print('<td nowrap><font color=red>None</font></td>')
        else:
            print('<td nowrap>', persons[p][3], '(', persons[p][2],')</td>')
        if persons[p][7] == 0:
            print('<td nowrap><font color=red>None</font></td>')
        else:
            print('<td nowrap>', persons[p][7], '(', persons[p][6],')</td>')
        if persons[p][4] == 0:
            print('<td nowrap><font color=red>None</font></td>')
        else:
            print('<td nowrap>', persons[p][4], '</td>')
        if persons[p][14] == 0:
            print('<td nowrap><font color=red>None</font></td>')
        else:
            print('<td nowrap>', persons[p][14], '</td>')
        print('<td>', persons[p][5], '</td>')
        print('<td class="total">', persons[p][1], '</td>')        
        ### if no shifts (RC and TRG) in the last 180 days and Total shifts is 0
        if (persons[p][15] == 0 and persons[p][1] == 0):
            print('<td nowrap><font color=red>Expired</font></td>')
        else:
            ## if no last RC shift, print the last TRG shift (in red if earlier than today so expired)
            if persons[p][16]==0:
               #if datetime.strptime(persons[p][17],"%d-%m-%Y") < datetime.now():
               #   print '<td nowrap><font color=red>', persons[p][17], '</font></td>'
               #else:
                  print('<td nowrap>', persons[p][17], '</td>')
            else:
               if (persons[p][17]!=0 and datetime.strptime(persons[p][16],"%d-%m-%Y") < datetime.strptime(persons[p][17],"%d-%m-%Y")):   
                  if datetime.strptime(persons[p][17],"%d-%m-%Y") < datetime.now():
                     print('<td nowrap><font color=red>', persons[p][17], '</font></td>')
                  else:
                     print('<td nowrap>', persons[p][17], '</td>')
               else:
                  if datetime.strptime(persons[p][16],"%d-%m-%Y") < datetime.now():
                     print('<td nowrap><font color=red>', persons[p][16], '</font></td>')
                  else:
                     print('<td nowrap>', persons[p][16], '</td>')

        print('</tr>')
    print('</table>')

def start_html(start, end, task):
	print('<html>')
	print('<head>')
	print('<title>', task, '</title>')
	print('</head>')
	print('<body onload="counters()">')

	print('<br>')
	print('<table align=center cellspacing=0 cellpadding=0 border=0>')
	print('<tr><td align=center><h3>', task, ' reports for period ',start.strftime("%d-%m-%Y"), '-->', end.strftime("%d-%m-%Y"),'</h3>')
	print('<h5><span id="count_shifters"></span><span id="hide-show" onclick="toggle()" style="cursor:pointer">(Hide)</span><br><span id="count_shifts"></span></h5>')

def end_html(persons,shifters_without_booking,taken,uncovered,first_uncovered):
  print('</table>')
  print('<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>')
  print('<script>')
  print('function counters() {')
  print('document.getElementById("count_shifters").innerHTML="Currently ',len(persons),' shifters and ',shifters_without_booking,' without any booked primary shift ";')
  print('document.getElementById("count_shifts").innerHTML="Uncovered shifts: ',uncovered,'; First uncovered shift: ',first_uncovered,'";')
  print('var title=document.title;')
  print('$(".total").each(function(index,item){')
  print('    if(parseInt($(item).html())==0){')
  print('       $(this).closest(\'tr\').attr("class","toggleit");}')
  print('    if(parseInt($(item).html())!=0 && ($(item).closest(\'tr\').find(\'td:eq(7)\').text()=="None" || $(item).closest(\'tr\').find(\'td:eq(8)\').text()=="None")){')
  print('       $(this).closest(\'tr\').css("background-color","#dec1b9");')
  print('    }')
  print('    if(parseInt($(item).html())!=0 && $(item).closest(\'tr\').find(\'td:eq(2)\').text().trim()=="Yes" && ($(item).closest(\'tr\').find(\'td:eq(7)\').text()=="None" || $(item).closest(\'tr\').find(\'td:eq(8)\').text()=="None")){')
  print('       $(this).closest(\'tr\').css("background-color","#e3e3b4");')
  print('    }')
  print('    if(parseInt($(item).html())!=0 && title.indexOf("Run Control")>=0 && $(item).closest(\'tr\').find(\'td:eq(2)\').text().trim()=="Yes" && $(item).closest(\'tr\').find(\'td:eq(7)\').text()=="None" && $(item).closest(\'tr\').find(\'td:eq(8)\').text()!="None"){')
  print('       $(this).closest(\'tr\').css("background-color","#f6f6f6");')
  print('    }')
  print('    if(parseInt($(item).html())!=0 && title.indexOf("Trigger")>=0 && $(item).closest(\'tr\').find(\'td:eq(2)\').text().trim()=="Yes" && $(item).closest(\'tr\').find(\'td:eq(8)\').text()=="None" && $(item).closest(\'tr\').find(\'td:eq(7)\').text()!="None"){')
  print('       $(this).closest(\'tr\').css("background-color","#f6f6f6");')
  print('    }')
  print('});')
  print('}')
  print('</script>')
  print('<script>')
  print('function toggle() {')
  print('if (document.getElementById("hide-show").innerHTML=="(Hide)") {document.getElementById("hide-show").innerHTML="(Show)";$(".toggleit").toggle();}')
  print('else if (document.getElementById("hide-show").innerHTML=="(Show)") {document.getElementById("hide-show").innerHTML="(Hide)";$(".toggleit").toggle();}')
  print('}')
  print('</script>')
  print('<script>')
  print('function updateTraining() {')
  print('var sname,toUpdate;')
  print('$(function(){')
  print('$(\'#report\').one(\'click\', \'td\', function(){')
  print('var this$   = $(this),')
  print('_status = !!this$.data(\'status\');')
  print('this$.html(_status ? \'No\' : \'Yes\').data(\'status\', !_status);')
  print('toUpdate=$(this).text(); pos=$(this).index();')
  print('sname = $(this).closest(\'tr\').find(\'td:eq(0)\').text(); ProcessSimpleCgi(sname,toUpdate,pos);')
  print('});')
  print('});')
  print('}')

  print('function ProcessSimpleCgi(sname,trained,pos)')
  print('{')
  print('    param1Data = sname;')
  print('    param2Data = trained; param3Data = pos;')
  print('    params = "param1=" + param1Data + "&param2=" + param2Data + "&param3=" + param3Data;')
  print('    $.ajax(')
  print('    {')
  print('        type: "POST",')
  print('        url: "/ssottoco/cgi-bin/UpdateTraining.py",')
  print('        data: params,')
  print('        dataType: "html",')
  print('        success: function (html)')
  print('        {')
  #print '            var params = $(html).filter(function(){ return $(this).is(\'p\') });'
  #print '            params.each('
  #print '                function()'
  #print '                {'
  #print '                    var value = "<li>" + $(this).html() + "</li>";'
  #print '                    $("#paramsList").append( value );'
  #print '                }'
  #print '            );'
  print('        },')
  print('        error: function(request, ajaxOptions, thrownError)')
  print('        {')
  print('            $("#debug").html(request.responseText);')
  print('        }')
  print('    });')
  print('}')

  print('</script>')	
  print('<script>')
  print('function sortTable(n) {')
  print('var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;')
  print('table = document.getElementById("report");')
  print('switching = true;')
  print('dir = "asc";')
  print('while (switching) {')
  print('switching = false;')
  print('rows = table.getElementsByTagName("TR");')
  print('for (i = 1; i < (rows.length - 1); i++) {')
  print('  shouldSwitch = false;')
  print('  x = rows[i].getElementsByTagName("TD")[n];')
  print('  y = rows[i + 1].getElementsByTagName("TD")[n];')
  print('  if (dir == "asc") {')
  print('    if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {')
  print('      shouldSwitch= true;')
  print('      break;')
  print('    }')
  print('  } else if (dir == "desc") {')
  print('    if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {')
  print('      shouldSwitch= true;')
  print('      break;')
  print('    }')
  print('  }')
  print('}')
  print('if (shouldSwitch) {')
  print('  rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);')
  print('  switching = true;')
  print('  switchcount ++;')      
  print('} else {')
  print('  if (switchcount == 0 && dir == "asc") {')
  print('    dir = "desc";')
  print('    switching = true;')
  print('  }}}}')
  print('</script>')
  print('<br>')
  print('Generated', datetime.now())
  print('</body>')
  print('</html>')

def readFile(shiftername):
    global expert,trcheck,sirRCcheck,sirTRGcheck,sirShRCcheck,sirShTRGcheck
    expert="No"
    trcheck="No"
    sirRCcheck="No"
    sirTRGcheck="No"
    sirShRCcheck="No"
    sirShTRGcheck="No"
    ispresent=False
    with open("/afs/cern.ch/user/s/ssottoco/www/otp_checks/data/persons_RC.csv","r") as inf:
        reader = csv.reader(inf)
        for row in reader:
            #currentrow=row.split(',')
            #print row
            if shiftername.strip()==row[0].strip():
                ispresent=True
                if row[1]!='':
                    expert=row[1]
                if row[2]!='': 
                    trcheck=row[2]
                if row[3]!='': 
                    sirRCcheck=row[3]
                if row[4]!='': 
                    sirTRGcheck=row[4]
                if row[5]!='': 
                    sirShRCcheck=row[5]
                if row[6]!='': 
                    sirShTRGcheck=row[6]
                break
    if ispresent==False:
       with open("/afs/cern.ch/user/s/ssottoco/www/otp_checks/data/persons_RC.csv","a") as inf:
         inf.write("%s,,,,,,\n"%shiftername)
               
    return expert,trcheck,sirRCcheck,sirTRGcheck,sirShRCcheck,sirShTRGcheck

if __name__ == '__main__':

    #start = datetime(2017, 1, 1)
    #end = datetime(2017, 12, 31)
    
    task1 = sys.argv[1]
    task2 = sys.argv[2]
    task3 = sys.argv[3]
    task4 = sys.argv[4]
    year = int(sys.argv[5])
    qualInterval= int(sys.argv[6])
    requirement = '' if len(sys.argv) < 8 else sys.argv[7]

    start = datetime(year, 1, 1)
    end = datetime(year, 12, 31)
    primaryT1 = OTPTask(task1, requirement)
    primaryT2 = OTPTask(task2, requirement)
    shadowT3 = OTPTask(task3,requirement)
    shadowT4 = OTPTask(task4, requirement)
    
    primaryT1shifters = primaryT1.shifters()
    primaryT2shifters = primaryT2.shifters()
    shadowT3shifters = shadowT3.shifters()
    shadowT4shifters = shadowT4.shifters()
    persons = defaultdict(lambda: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0])
    now = datetime.today()
    today = datetime(now.year, now.month, now.day)
    ## disqualification if no shifts in the last 6 months
    ## adding 2 months YETS to the expiration policy (Jan-Febr 2018)
    qualificationlimit = now - timedelta(days=qualInterval)

    shifters_without_booking = 0

    for shifter in primaryT1shifters:
        primaryT1shifts = primaryT1.shifts(shifter['ID'], start, end)
        qualifT1check=primaryT1.shifts(shifter['ID'],qualificationlimit, today)
        if shifter in primaryT2shifters:
           qualifT2check = primaryT2.shifts(shifter['ID'],qualificationlimit, today)
           primaryT2shiftsdone = primaryT2.shifts(shifter['ID'], start, today)
           primaryT2shifts = primaryT2.shifts(shifter['ID'], start, end)
        else:
           qualifT2check=[]
           primaryT2shiftsdone=[]
           primaryT2shifts=[]
        primaryT1shiftsdone = primaryT1.shifts(shifter['ID'], start, today)
        shadowT3shifts = shadowT3.shifts(shifter['ID'], start, end)
        shadowT4shifts = shadowT4.shifts(shifter['ID'], start, end)

        sname = shifter['LAST_NAME']+' '+shifter['FIRST_NAME']
        #print sname,primaryT1shifts
        semail = shifter['EMAIL']
        expert,trcheck,sirRCcheck,sirTRGcheck,sirShRCcheck,sirShTRGcheck=readFile(sname)
        persons[sname][13]=expert
        persons[sname][8]=trcheck
        persons[sname][9]=sirRCcheck
        persons[sname][10]=sirTRGcheck
        persons[sname][11]=sirShRCcheck
        persons[sname][12]=sirShTRGcheck
        persons[sname][0] = semail
        persons[sname][1] = len(primaryT1shifts)
        if persons[sname][1] == 0:
            shifters_without_booking+=1
        persons[sname][2] = len(shadowT3shifts)
        persons[sname][5] = len(primaryT1shiftsdone)
        persons[sname][6] = len(shadowT4shifts)
        persons[sname][15]=len(qualifT1check+qualifT2check)

        for s in sorted(primaryT1shifts, key = lambda k: k['DT']):
            firstprimary=s['DT'].strftime("%d-%m-%Y")+' '+str(s['HOUR_FROM'])+'-'+str(s['HOUR_TO'])
            #print sname, firstprimary, "FIRST"
            persons[sname][4] = firstprimary
            break
        for s in sorted(primaryT1shifts, key = lambda k: k['DT'], reverse=True):
            lastprimary=s['DT'].strftime("%d-%m-%Y")+' '+str(s['HOUR_FROM'])+'-'+str(s['HOUR_TO'])
            #print sname,lastprimary,"LAST"
            persons[sname][14] = lastprimary
            qualifExpDate=s['DT']+timedelta(days=qualInterval)
            persons[sname][16] = qualifExpDate.strftime("%d-%m-%Y")
            break
        if shifter in primaryT2shifters:
            for s in sorted(primaryT2shifts, key = lambda k: k['DT'], reverse=True):
               lastprimary=s['DT'].strftime("%d-%m-%Y")+' '+str(s['HOUR_FROM'])+'-'+str(s['HOUR_TO'])
               qualifExpDate=s['DT']+timedelta(days=qualInterval)
               #print sname,qualifExpDate
               persons[sname][17] = qualifExpDate.strftime("%d-%m-%Y")
               break
        for s in sorted(shadowT3shifts, key = lambda k: k['DT'], reverse=True):
            lastRCshadow=s['DT'].strftime("%d-%m-%Y")+' '+str(s['HOUR_FROM'])+'-'+str(s['HOUR_TO'])
            persons[sname][3] = lastRCshadow
            break
        for s in sorted(shadowT4shifts, key = lambda k: k['DT'], reverse=True):
            lastTRGshadow=s['DT'].strftime("%d-%m-%Y")+' '+str(s['HOUR_FROM'])+'-'+str(s['HOUR_TO'])
            persons[sname][7] = lastTRGshadow
            break
   
    taken, uncovered, date_uncovered = primaryT1.covered(end)
    if len(date_uncovered)>0:
        first_uncovered=date_uncovered[0].strftime("%d-%m-%Y")
    else:
        first_uncovered=0
 
    header()

    start_html(start, end, task1)
    
    format_shifters(persons)

    end_html(persons,shifters_without_booking,taken,uncovered,first_uncovered)


