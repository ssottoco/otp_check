#!/usr/bin/env tdaq_python

from __future__ import print_function
from builtins import zip
from builtins import map
from builtins import range
from builtins import object
from otpapi import OTPAPI
from datetime import datetime, timedelta, date
from operator import itemgetter
from itertools import groupby

class OTPTask(object):

    def __init__(self, taskname, requirement=None):
        self.otp = OTPAPI()
        self.name = taskname
        self.requirement = requirement

    def shifters(self):
        shifters = self.otp.get_shifters(self.name, self.requirement)
        return shifters

    def shifts(self, shifter_id, start, end):
        shifts = self.otp.get_shifts(shifter_id, self.name,
                                     start, end, self.requirement)

        #for s in shifts:
        #    for k in ['HOUR_FROM', 'HOUR_TO', 'ALLOCATED_PERSON_ID']:
        #        s.pop(k)

        return shifts

    def covered(self, end):

        now = datetime.today()
        today = datetime(now.year, now.month, now.day)

        coverage =\
            self.otp.get_coverage(self.name, today, end)
        
        coverage = sorted(c['DT'] for c in coverage)
        taken = len(coverage)
        #print taken
        uncovered=0 
        date_uncovered=[]
        coverage_groupedByDay = [len(list(j)) for i, j in groupby(coverage)]
        #print coverage_groupedByDay
        for i, val in enumerate(coverage_groupedByDay):
            if val < 3:
                date_uncovered.append(coverage[i*3])
                break
        for i, val in enumerate(coverage_groupedByDay):
            if val == 0:
                uncovered +=3
            elif val == 1:
                uncovered +=2
            elif val == 2:
                uncovered +=1
            #print i, val, uncovered,';'
        
        #print len(coverage_groupedByDay)
        #print uncovered
        return (taken,uncovered,date_uncovered)

    def gaps(self, end):

        now = datetime.today()
        today = datetime(now.year, now.month, now.day)

        coverage =\
            self.otp.get_coverage(self.name, today, end)
        coverage = sorted(c['DT'] for c in coverage)

        # Split the list in consecutive groups
        # We use an incrementing counter to detect holes
        # i.e. the difference between the data and the counter
        # is constant as long as there are no holes
        def grouper(x):
            idx, date = x
            return (today + timedelta(days=idx)) - date

        coverage = [list(map(itemgetter(1), g))
                    for k, g in groupby(enumerate(coverage), grouper)]

        # Calculate gaps between groups
        pairs =\
            zip(coverage+[[end+timedelta(days=1), ]], [[today, ], ]+coverage)
        gaps = [(b[-1], (a[0]-b[-1])) for a, b in pairs]

        # Remove empty gaps
        gaps = [g for g in gaps if g[0]]

        # Expand gaps into sequence of dates
        gaps = [[g[0]+timedelta(days=(i+1)) for i in range(g[1].days-1)]
                for g in gaps]
        return gaps

if __name__ == '__main__':

    import sys

    task = 'ROS hardware intervention' if len(sys.argv) < 2 else sys.argv[1]

    oncall = OTPTask(task)

    rosshifters = oncall.shifters()

    print(rosshifters)

    # 1543 is Wainer Vandelli
    shifts = oncall.shifts(1543,
                           datetime(2015, 1, 1), datetime(2015, 12, 31))
    print(shifts)

    gaps = oncall.gaps(datetime(2015, 12, 31))
    print(gaps)
